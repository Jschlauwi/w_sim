
# import Rendering_Stuff.surfaces as SF
# import pyqtgraph as pg
# from PyQt5 import QtCore, QtGui
# import numpy as np




# class WorldWindow(pyglet.window.Window):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         self.set_minimum_size(400, 300)
#         glClearColor(0.2, 0.3, 0.2, 1.0)
#
#         # self.triangle = Triangle()
#         self.quad = SF.Quad3()
#
#     def on_draw(self):
#         self.clear()
#         # self.triangle.verticies.draw(GL_TRIANGLES)
#         # self.quad.vertices.draw(GL_TRIANGLES)
#         self.quad.render()
#
#     def on_resize(self, width, height):
#         glViewport(0, 0, width, height)
#
# class WorldGrid:
#     def __init__(self, title="Window", resolution=(200,200), win_dim=(800, 800), bgnd_col='k', fgnd_col='w'):
#
#         pg.setConfigOption('leftButtonPan', True)
#         pg.setConfigOption('background', bgnd_col)
#         pg.setConfigOption('foreground', fgnd_col)
#         pg.setConfigOptions(antialias=False)
#         # Interpret image data as row-major instead of col-major
#         pg.setConfigOptions(imageAxisOrder='row-major')
#         pg.mkQApp()
#         self.win = pg.GraphicsLayoutWidget()
#
#         # set window title
#         self.win.setWindowTitle(title)
#
#
#         # ----------- make maps ---------------------------
#         self.def_map_dim = resolution
#         # self.map = np.zeros([4] + list(self.def_map_dim))
#         self.map = np.zeros(list(self.def_map_dim) + [4])
#         self.plt = self.win.addPlot(title="", rowspan=1,
#                                              colspan=1,
#                                              row=1,
#                                              col=1)
#
#         self.img = pg.ImageItem()
#         self.img.setImage(self.map)
#         self.plt.addItem(self.img)
#         # zoom to fit image
#         self.plt.autoRange()
#         # -------------------------------------------------
#
#         self.gen_window(win_dim[0], win_dim[1])
#
#
#     def __rescale_map(self, m_id, to_res, frm_res=()):
#         """
#         returns a map with specified resolution
#         :param m_id:
#         :param to_res:
#         :param frm_res:
#         :return:
#         """
#         # initialize the new map to the maximum resolution
#         new_map = np.zeros(to_res)
#
#         # if the old resolution is not given then get it
#         if len(frm_res) != 2:
#             frm_res = np.shape(self.map[m_id])
#
#         # calculate the scaling factor for each dimension
#         factr = [a / b for a, b in zip(frm_res, to_res)]
#
#         for i in range(to_res[0]):
#             # ensure the smaller index does not go out of bounds
#             # due to round off errors
#             i_frm = max(frm_res[0] - 1, int(i * factr[0]))
#             for j in range(to_res[1]):
#                 j_frm = max(frm_res[1] - 1, int(j * factr[1]))
#                 new_map[i][j] = self.map[m_id][i_frm][j_frm]
#         # return the map with new resolution
#         return new_map
#
#     def clear(self):
#         # self.map = np.zeros([4] + list(self.def_map_dim))
#         self.map = np.zeros(list(self.def_map_dim) + [4])
#         return
#
#     def add_layer(self, rgba, grid):
#         # print(np.shape(self.map))
#         self.map += np.einsum('ij,k->ijk', grid, rgba)
#         # print(np.shape(self.map))
#
#     def refresh_image(self):
#         ## Display the data
#
#         self.img.setImage(self.map)
#
#         QtCore.QTimer.singleShot(1, self.refresh_image)
#
#     def gen_window(self, xdim=800, ydim=800):
#         self.win.resize(xdim, ydim)
#         self.win.show()
#
#     def get_map_dims(self):
#         return list(self.def_map_dim)


if __name__ == '__main__':
    print("Start")
    print(os.getcwd())
    # window = WorldWindow(1280, 720, "World Window", resizable=True)
    # pyglet.app.run()
    # window = WorldGrid(title="World Window")
    #
    # # window.on_draw()
    # window.refresh_image()
    #
    # dims = window.get_map_dims()[:2]
    # data = np.random.normal(size=dims)
    # data[20:80, 20:80] += 2.
    # data = pg.gaussianFilter(data, (3, 3))
    # data += np.random.normal(size=dims) * 0.1
    #
    # window.add_layer(rgba=(0.9, 0.9, 1.0, 1.0), grid=data)
    # # todo add interface to select rgba
    # # todo include different refresh methods
    # # todo include object overlay and object sight
    #
    # if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
    #     QtGui.QApplication.instance().exec_()

    print("End")