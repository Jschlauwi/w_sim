import os
import numpy as np
import Surf_visual as Sv

class SimMap:
    def __init__(self, map=None, dims=None):

        if isinstance(map, (np.ndarray, tuple, list, dict)):
            self.dimensions = np.shape(map)
            self.surface = {"base": map}
        elif isinstance(dims, (np.ndarray, tuple, list, dict)):
            self.dimensions = dims
            self.surface = {"base": np.zeros(dims, dtype=bool)}
        else:
            raise EOFError("unknown type: ", type(map), " or ", type(dims))

    def clear_layer(self, layer_id):
        self.surface[layer_id] = np.zeros(self.dimensions, dtype=bool)
        return

    def settle_down(self, surf, vec_id):
        if len(vec_id) > 1:
            self.settle_down(surf=surf[vec_id[0]], vec_id=vec_id[1:])
        else:
            surf[vec_id[0]] = True
        return

    def place_items(self, zipped_id_coord):
        for layer_id, vec_ind in zipped_id_coord:
            if layer_id not in self.surface.keys():
                self.clear_layer(layer_id=layer_id)
            self.settle_down(surf=self.surface[layer_id], vec_id=vec_ind)
        return

    def update_layer(self, layer_id, coord_list):
        self.clear_layer(layer_id=layer_id)
        for vec_ind in coord_list:
            if layer_id not in self.surface.keys():
                self.clear_layer(layer_id=layer_id)
            self.settle_down(surf=self.surface[layer_id], vec_id=vec_ind)
        return

    def get_surf(self, layer_id="base"):
        return self.surface[layer_id]


def temp_surf(dims):
    # generate surface map
    data = np.random.normal(size=dims)
    data[20:80, 20:80] += 2.
    data = Sv.pg.gaussianFilter(data, (3, 3))
    data += np.random.normal(size=dims) * 0.1
    return data

def gen_item_list(count, dims):
    agnts = []
    for ind in range(count):
        tmp = []
        for dim in range(len(dims)):
            tmp.append(np.random.randint(low=0, high=dims[dim]))
        agnts.append(np.array(tmp))
    return agnts

if __name__ == '__main__':
    print("Start")
    print(os.getcwd())
    dims = (100,200)
    window = Sv.WorldGrid(title="World Window", resolution=dims)

    simwrld = SimMap(map=temp_surf(dims=dims))
    simwrld.update_layer(layer_id="agents", coord_list=gen_item_list(count=50, dims=dims))

    window.refresh_image()
    window.clear()

    window.add_layer(rgba=(0.9,0,0,1), grid=simwrld.get_surf(layer_id="base"))
    window.add_layer(rgba=(0,0.6,1,1), grid=simwrld.get_surf(layer_id="agents"))

    # todo add interface to select rgba
    # todo include different refresh methods
    # todo include object overlay and object sight

    if (Sv.sys.flags.interactive != 1) or not hasattr(Sv.QtCore, 'PYQT_VERSION'):
        Sv.QtGui.QApplication.instance().exec_()
    print("End")