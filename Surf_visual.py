import os
import sys
import pyglet
from pyglet.window import FPSDisplay
import pymunk
from pymunk.pyglet_util import DrawOptions

class GameWindow(pyglet.window.Window):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.set_location(x=300, y=50)
        self.fps = FPSDisplay(self)

        self.space = pymunk.Space()
        self.options = DrawOptions()

    def on_draw(self):
        self.clear()
        self.space.debug_draw(self.options)
        self.fps.draw()

    def update(self, dt):
        self.space.step(dt=dt)


if __name__ == '__main__':
    print("Start")
    print(os.getcwd())
    window = GameWindow(1280, 900, "Breakout", resizable=False)
    pyglet.clock.schedule_interval(window.update, 1/60.0)
    pyglet.app.run()

    print("End")