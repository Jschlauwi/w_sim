import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import pyqtSlot


def on_button_clicked():
    alert = QMessageBox()
    alert.setText('You clicked the button!')
    alert.exec_()


class App(QWidget):
    def __init__(self):
        super().__init__()
        self.title = 'World Visualization'
        self.left = 100
        self.top = 100
        self.width = 640
        self.height = 480
        self.layout = QVBoxLayout()
        self.initUI()

    def initUI(self):

        top_button = QPushButton('Top')
        top_button.clicked.connect(on_button_clicked)
        self.layout.addWidget(top_button)
        self.layout.addWidget(QPushButton('Bottom'))

        # Create textbox
        self.__add_text_box()
        self.__add_text_box()
        self.__add_text_box()
        confirm_button = QPushButton('Confirm')
        confirm_button.clicked.connect(self.on_click)
        self.layout.addWidget(confirm_button)

        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setLayout(self.layout)

        self.getInteger()
        self.getText()
        self.getDouble()
        self.getChoice()

        self.show()

    def __add_text_box(self):
        textbox = QLineEdit(self)
        textbox.move(0, 20)
        textbox.resize(40,40)
        self.layout.addWidget(textbox)

    def getInteger(self):
        i, okPressed = QInputDialog.getInt(self, "Get integer", "Percentage:", 28, 0, 100, 1)
        if okPressed:
            print(i)

    def getDouble(self):
        d, okPressed = QInputDialog.getDouble(self, "Get double", "Value:", 10.50, 0, 100, 10)
        if okPressed:
            print(d)

    def getChoice(self):
        items = ("Red", "Blue", "Green")
        item, okPressed = QInputDialog.getItem(self, "Get item", "Color:", items, 0, False)
        if okPressed and item:
            print(item)

    def getText(self):
        text, okPressed = QInputDialog.getText(self, "Get text", "Your name:", QLineEdit.Normal, "")
        if okPressed and text != '':
            print(text)

    @pyqtSlot()
    def on_click(self):
        textboxValue = self.textbox.text()
        QMessageBox.question(self, 'Message - pythonspot.com', "You typed: " + textboxValue, QMessageBox.Ok, QMessageBox.Ok)
        self.textbox.setText("")

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setStyle('Fusion')
    # app.setStyleSheet("QPushButton { margin: 1ex; }")
    window = App()

    layout = QVBoxLayout()
    top_button = QPushButton('Top')
    top_button.clicked.connect(on_button_clicked)
    layout.addWidget(top_button)
    layout.addWidget(QPushButton('Bottom'))
    window.setLayout(layout)
    window.show()
    sys.exit(app.exec_())