from pyglet.gl import *
import OpenGL.GL.shaders
import ctypes

class TriangleShader:
    def __init__(self):
        self.triangle = [-0.5, -0.5, 0.0, 1.0, 0.0, 0.0,
                         0.5, -0.5, 0.0, 0.0, 1.0, 0.0,
                         0.0, 0.5, 0.0, 0.0, 0.0, 1.0]
        # 6 vectors (pos, col, pos, col,...) * 3 dimensions-per-vector * 4 bytes-per-dimension

        self.vertex_shader_source = b"""
        #version 330
        in layout(location = 0) vec3 position;
        in layout(location = 1) vec3 color;
        
        out vec3 newColor;
        void main()
        {
            gl_position = vec4(position, 1.0f);
            newColor = color;
        }
        """
        self.fragment_shader_source = b"""
        #version 330
        in vec3 newColor;
        
        out vec4 outColor;
        void main()
        {
            outColor = vec4(newColor, 1.0f);
        }
        """

        # shader = OpenGL.GL.shaders.compileProgram(OpenGL.GL.shaders.compileShader(self.vertex_shader_source, GL_VERTEX_SHADER),
        #                                           OpenGL.GL.shaders.compileShader(self.fragment_shader_source, GL_FRAGMENT_SHADER))

        vertex_buff = ctypes.create_string_buffer(self.vertex_shader_source)
        c_vertex = ctypes.cast(ctypes.pointer(ctypes.pointer(vertex_buff)), ctypes.POINTER(ctypes.POINTER(GLchar)))
        vertex_shader = glCreateShader(GL_VERTEX_SHADER)
        glShaderSource(vertex_shader, 1, c_vertex, None)
        glCompileShader(vertex_shader)

        frag_buff = ctypes.create_string_buffer(self.fragment_shader_source)
        c_frag = ctypes.cast(ctypes.pointer(ctypes.pointer(frag_buff)), ctypes.POINTER(ctypes.POINTER(GLchar)))
        frag_shader = glCreateShader(GL_FRAGMENT_SHADER)
        glShaderSource(frag_shader, 1, c_frag, None)
        glCompileShader(frag_shader)

        shader = glCreateProgram()
        glAttachShader(shader, vertex_shader)
        glAttachShader(shader, frag_shader)
        glLinkProgram(shader)

        glUseProgram(shader)

        vbo = GLuint(0)
        glGenBuffers(1, vbo)

        # 72 is the bytes needed to stor the vectors
        # third item unpacks triangle into array
        glBindBuffer(GL_ARRAY_BUFFER, vbo)
        glBufferData(GL_ARRAY_BUFFER, 72, (GLfloat * len(self.triangle))(*self.triangle), GL_STATIC_DRAW)

        # positions
        # 24 is the bytes needed for each color-position vector pair
        glVertexAttribPointer(0,3, GL_FLOAT, GL_FALSE, 24, ctypes.c_void_p(0))
        glEnableVertexAttribArray(0)
        # colors
        # 12 is the bytes offset for the color vector
        glVertexAttribPointer(1,3, GL_FLOAT, GL_FALSE, 24, ctypes.c_void_p(12))
        glEnableVertexAttribArray(1)


class Triangle:
    def __init__(self):
        # [x1,y1,z1,x2,y2,z2,x3,y3,z3] c3B B== 0 to 255
        self.verticies = pyglet.graphics.vertex_list(3, ('v3f', [-0.5, -0.5, 0.0,
                                                                 0.5, -0.5, 0.0,
                                                                 0.0, 0.5, 0.0]),
                                                     ('c3B', [100, 200, 220,
                                                              200, 110, 100,
                                                              100, 250, 100]))


class Quad:
    def __init__(self):
        # [x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4] c3f = 0.0 to 1.0
        self.vertices = pyglet.graphics.vertex_list_indexed(4, [0, 1, 2,
                                                                2, 3, 0],
                                                            ('v3f', [-0.5, -0.5, 0.0,
                                                                        0.5, -0.5, 0.0,
                                                                        0.5, 0.5, 0.0,
                                                                        -0.5, 0.5, 0.0]),
                                                            ('c3f', [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0]))


class Quad2:
    def __init__(self):
        # [x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4] c3f = 0.0 to 1.0
        self.vert_order = [0, 1, 2, 2, 3, 0]
        self.vertex = [-0.5, -0.5, 0.0, 0.5, -0.5, 0.0, 0.5, 0.5, 0.0, -0.5, 0.5, 0.0]
        self.color = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0]
        self.vertices = pyglet.graphics.vertex_list_indexed(4, self.vert_order,
                                                            ('v3f', self.vertex), ('c3f', self.color))


class Quad3:
    def __init__(self):
        # [x1,y1,z1,x2,y2,z2,x3,y3,z3,x4,y4,z4] c3f = 0.0 to 1.0
        self.vert_order = [0, 1, 2, 2, 3, 0]
        self.vertex = [-0.5, -0.5, 0.0, 0.5, -0.5, 0.0, 0.5, 0.5, 0.0, -0.5, 0.5, 0.0]
        self.color = [1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0]

    def render(self):
        self.vertices = pyglet.graphics.draw_indexed(4, GL_TRIANGLES, self.vert_order,
                                                     ('v3f', self.vertex), ('c3f', self.color))